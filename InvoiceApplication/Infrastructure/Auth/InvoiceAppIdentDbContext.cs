﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApplication.Infrastructure.Auth
{
    public class InvoiceAppIdentDbContext : IdentityDbContext<InvoiceAppUserTwo>
    {
        public InvoiceAppIdentDbContext() : base("InvoiceAppDbConnection")
        {
            //force the initialization of the database upon start
            Database.Initialize(true);
        }

        /// <summary>
        /// Factory method required by OWIN to instantiate the sports store identity db context
        /// </summary>
        /// <returns></returns>
        public static InvoiceAppIdentDbContext Create()
        {
            return new InvoiceAppIdentDbContext();
        }
    }
}