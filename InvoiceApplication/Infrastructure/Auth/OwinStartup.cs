﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Cookies;
using SportsStore.WebApp.Infrastructure.Auth;

[assembly: OwinStartup(typeof(InvoiceApplication.Infrastructure.Auth.OwinStartup))]

namespace InvoiceApplication.Infrastructure.Auth
{
    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            //create the database context for Identity
            app.CreatePerOwinContext<InvoiceAppIdentDbContext>(InvoiceAppIdentDbContext.Create);

            //create the identity user manager
            app.CreatePerOwinContext<InvoiceAppUserManager>(InvoiceAppUserManager.Create);

            //configure the cookie authentication and the login page
            //configure authentication
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/UserAccounts")
            });
        }
    }
}
