﻿using InvoiceApplication.Infrastructure.Auth;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace SportsStore.WebApp.Infrastructure.Auth
{
    public class InvoiceAppUserManager:UserManager<InvoiceAppUserTwo>
    {
        public InvoiceAppUserManager(IUserStore<InvoiceAppUserTwo> store) : base(store)
        {
            
        }
        

        public static InvoiceAppUserManager Create(
        IdentityFactoryOptions<InvoiceAppUserManager> options,
        IOwinContext context)
        {
            InvoiceAppIdentDbContext dbContext = context.Get<InvoiceAppIdentDbContext>();
            InvoiceAppUserManager userManager = new InvoiceAppUserManager(new UserStore<InvoiceAppUserTwo>(dbContext));
            return userManager;
        }
    }
}