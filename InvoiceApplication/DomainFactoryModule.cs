﻿using InvoiceApplication.Models;
using InvoiceApplication.Persistence;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApplication
{
    public class DomainFactoryModule:NinjectModule
    {
        public override void Load()
        {
            Bind<IInvoiceList>().To<InvoiceList>();
            Bind<IInvoiceRepository>().To<DatabaseInvoiceRepository>();
            Bind<IUserList>().To<UserList>();
        }
    }
}