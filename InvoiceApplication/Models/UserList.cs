﻿using InvoiceApplication.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApplication.Models
{
    class UserList : IUserList
    {
        private IInvoiceRepository _repository;

        public IEnumerable<User> Users { get; }
        public UserList(IInvoiceRepository repository)
        {
            _repository = repository;

            this.Users = _repository.UserList;
        }

        public User CheckUser(User user)
        {
            return _repository.CheckUser(user);
        }
    }
}