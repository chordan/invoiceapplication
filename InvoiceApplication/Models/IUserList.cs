﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApplication.Models
{
    public interface IUserList
    {
        IEnumerable<User> Users { get; }
        User CheckUser(User user);
    }
}
