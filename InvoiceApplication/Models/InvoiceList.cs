﻿using InvoiceApplication.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApplication.Models
{
    /// <summary>
    /// Methods in here use the IInvoiceRepository interface,
    /// which uses the DatabaseInvoiceRepository class for implementation of the methods.
    /// </summary>
    class InvoiceList: IInvoiceList
    {
        private IInvoiceRepository _repository;

        public IEnumerable<Invoice> Invoices { get; }

        public InvoiceList(IInvoiceRepository repository)
        {
            _repository = repository;

            this.Invoices = _repository.InvoiceList;
        }
        

        public void SaveInvoice(Invoice invoice)
        {
            _repository.SaveInvoice(invoice);
        }

        public void DeleteInvoice(Invoice invoice)
        {
            _repository.DeleteInvoice(invoice);
        }
    }
}