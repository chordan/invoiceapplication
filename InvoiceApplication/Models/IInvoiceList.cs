﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApplication.Models
{
    /// <summary>
    /// Interface for the InvoiceList
    /// </summary>
    public interface IInvoiceList
    {
        IEnumerable<Invoice> Invoices { get; }

        /// <summary>
        /// Takes an invoice object and saves it to our Invoice table in the DB
        /// </summary>
        /// <param name="invoice">Invoice to save/add into the DB</param>
        void SaveInvoice(Invoice invoice);

        /// <summary>
        /// Deletes an invoice from the DB matching the one given.
        /// But now I read that this is not necessary; so it's not going to be used,
        /// rather invoices will use the SaveInvoice and change the Paid Bool
        /// </summary>
        /// <param name="invoice">Invoice to delete from DB</param>
        void DeleteInvoice(Invoice invoice);
    }
}