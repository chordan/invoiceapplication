﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InvoiceApplication.Models
{
    public enum CurrencyType {
        CAD=1,
        US=2,
        EUR=3 }
    public class Invoice
    {
        [Key]
        public int InvoiceNumber { get; set; }

        [Required(ErrorMessage = "Please enter the client name")]
        public string ClientName { get; set; }

        [Required(ErrorMessage = "Please enter the client address")]
        public string ClientAddress { get; set; }

        
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateOfShipment { get; set; }

        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PaymentDueDate { get; set;}

        [Required(ErrorMessage = "Please enter the product name")]
        public string ProductName { get; set; }

        
        public int ProductQuantity { get; set; }

        [DisplayFormat(DataFormatString = "{0:C0}")]
        public decimal UnitPrice { get; set; }
        public CurrencyType Currency { get; set; }

        public bool Paid { get; set; }

        internal void Change(Invoice invoice)
        {
            this.ClientName = invoice.ClientName;
            this.ClientAddress = invoice.ClientAddress;
            this.DateOfShipment = invoice.DateOfShipment;
            this.PaymentDueDate = invoice.PaymentDueDate;
            this.ProductName = invoice.ProductName;
            this.ProductQuantity = invoice.ProductQuantity;
            this.UnitPrice = invoice.UnitPrice;
            this.Currency = invoice.Currency;
            this.Paid = invoice.Paid;
        }
    }
}