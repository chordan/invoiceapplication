﻿using InvoiceApplication.Infrastructure.Auth;
using InvoiceApplication.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SportsStore.WebApp.Infrastructure.Auth;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace InvoiceApplication.Controllers
{
    
    public class UserAccountsController : Controller
    {

        private IUserList _users;

        public UserAccountsController(IUserList users)
        {
            _users = users;
        }

        /// <summary>
        /// Provides access to UserManager
        /// </summary>
        private InvoiceAppUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<InvoiceAppUserManager>(); }
        }

        /// <summary>
        /// Provides access to Authentication
        /// </summary>
        private IAuthenticationManager AuthManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }


        private void CheckForUserRegistration(User user)
        {
            //check if this is the first user
            if (this.UserManager.Users.Count() == 0)
            {
                //no users are currently created, create this first one as the administrator
                InvoiceAppUserTwo firstUser = new InvoiceAppUserTwo { UserName = user.UserName };
                
                //create an identity for the user by supplying the password that will be 
                //salted and hashed by the Identity framework
                IdentityResult result = this.UserManager.Create(firstUser, user.Password);
                this.UserManager.AddToRole(firstUser.Id, "Administrator");
                //more error checking could be implemented here to display errors to the user
            }
            else if (this.UserManager.Users.FirstOrDefault(u=>u.UserName==user.UserName) == null)
            {
                InvoiceAppUserTwo newUser = new InvoiceAppUserTwo { UserName = user.UserName };
                //create an identity for the user by supplying the password that will be 
                //salted and hashed by the Identity framework
                IdentityResult result = this.UserManager.Create(newUser, user.Password);

                UserManager.AddToRole(newUser.Id, "User");
            }

        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            AuthManager.SignOut();
            return View("LoginView");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(User vmUser, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //if no users exist in the system register this user automatically
                //as a simple mechanism to add the "admin" user of the system
                CheckForUserRegistration(vmUser);

                //Use the store manager to check the user credentials
                InvoiceAppUserTwo user = UserManager.Find(vmUser.UserName, vmUser.Password);
                if (user != null)
                {
                    //create an identity to represent this valid user
                    ClaimsIdentity ident = UserManager.CreateIdentity(user,
                                               DefaultAuthenticationTypes.ApplicationCookie);

                    //clear any previous authentication
                    AuthManager.SignOut();

                    //sign the user in using the identity that was created for it
                    AuthManager.SignIn(ident);

                    //redirect to the URL that was requested if one was requested, otherwise
                    //go a default URL, StoreAdmin/Index. ?? is the null-coalescing operator
                    //which provides a short syntax for checking for null
                    

                    if (UserManager.IsInRole(user.Id,"Manager")|| UserManager.IsInRole(user.Id, "Administrator"))
                    {
                        return Redirect(returnUrl ?? Url.Action("Index", "ManagerInvoice"));
                    }
                    else
                    {
                        return Redirect(returnUrl ?? Url.Action("Index", "BareUserInvoice"));
                    }

                    
                }
                else
                {
                    //the user authentication failed, display the login view with the
                    //appropriate errors
                    ModelState.AddModelError("", "User name or password are incorrect");
                    ViewBag.ReturnUrl = returnUrl;
                    return View("LoginView",vmUser);
                }
            }
            else
            {
                return View("LoginView",vmUser);
            }

        }

        
        public ActionResult BackToActions()
        {
            return View("ActionsView");
        }
    }
}