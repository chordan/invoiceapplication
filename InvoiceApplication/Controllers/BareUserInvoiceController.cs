﻿using InvoiceApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InvoiceApplication.Controllers
{
    
    [Authorize(Roles = "User, Manager, Administrator" )]
    public class BareUserInvoiceController : Controller
    {
        
        private IInvoiceList _invoices;
        public BareUserInvoiceController(IInvoiceList invoices)
        {
            _invoices = invoices;
        }


        public ActionResult Index()
        {
            return View("ActionsView");
        }

        public ActionResult AddInvoice()
        {

            return View("AddEditInvoiceView");
        }

        public ActionResult SubmitInvoice(Invoice invoicemodel)
        {
            invoicemodel.Paid = false;
            if (ModelState.IsValid)
            {
                int newInvoiceNumber = 0;

                List<Invoice> cachelist = new List<Invoice>();

                foreach (Invoice compareInv in _invoices.Invoices)
                {
                    if (compareInv.InvoiceNumber >= newInvoiceNumber)
                    {
                        newInvoiceNumber = compareInv.InvoiceNumber + 1;
                    }
                }
                invoicemodel.InvoiceNumber = newInvoiceNumber;

                cachelist.Add(invoicemodel);
                _invoices.SaveInvoice(invoicemodel);

                return RedirectToAction("Index", "BareUserInvoice");
            }
            else
            {
                return View("AddEditInvoiceView");
            }
        }
    }
}