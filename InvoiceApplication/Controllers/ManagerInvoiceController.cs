﻿using InvoiceApplication.Models;
using InvoiceApplication.Persistence;
using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace InvoiceApplication.Controllers
{
    [Authorize(Roles ="Administrator, Manager")]
    public class ManagerInvoiceController : Controller
    {

        private IInvoiceList _invoices;

        

        public ManagerInvoiceController(IInvoiceList invoices)
        {
            _invoices = invoices;
        }

        
        public ActionResult Index()
        {
            return View("ReviewReceivablesView", _invoices.Invoices);
        }

        // GET: Invoice
        public ActionResult AddInvoice()
        {
            
            return View("AddEditInvoiceView");
        }

        

        public ActionResult EditInvoice()
        {
            return View("AddEditInvoiceView");
        }

        public ActionResult SaveInvoice(Invoice changedInvoice)
        {
            if (changedInvoice.Currency != 0)
            {
                _invoices.SaveInvoice(changedInvoice);
                return View("ReviewReceivablesView", _invoices.Invoices);
            }
            else
            {
                return View("AddEditInvoiceView", changedInvoice);
            }
            
        }

        public ActionResult PaidInvoice(Invoice paidInvoice)
        {
            Invoice foundInvoice = _invoices.Invoices.FirstOrDefault(p => p.InvoiceNumber == paidInvoice.InvoiceNumber);
            if (foundInvoice != null)
            {
                //don't delete, just save the invoice with the paid bool changed
                paidInvoice.Paid = true;
                _invoices.SaveInvoice(paidInvoice);
                return View("ReviewReceivablesView",_invoices.Invoices);
            }
            else
            {
                //display an error

                //redirect to the list view
                return View("ReviewReceivablesView",_invoices.Invoices);
            }
        }

        public ActionResult SubmitInvoice(Invoice invoicemodel)
        {
            invoicemodel.Paid = false;
            if (ModelState.IsValid)
            {
                int newInvoiceNumber=0;

                List<Invoice> cachelist = new List<Invoice>();
                
                    foreach (Invoice compareInv in _invoices.Invoices)
                    {
                        if (compareInv.InvoiceNumber >= newInvoiceNumber)
                        {
                            newInvoiceNumber = compareInv.InvoiceNumber + 1;
                        }
                    }
                invoicemodel.InvoiceNumber = newInvoiceNumber;

                cachelist.Add(invoicemodel);
                _invoices.SaveInvoice(invoicemodel);

                return RedirectToAction("Index","ManagerInvoice");
            }
            else
            {
                return View("AddEditInvoiceView");
            }
        }
        
        public ActionResult ManageInvoice(Invoice invoicemodel)
        {
            return View("AddEditInvoiceView",invoicemodel);
        }
        
        public ActionResult ReviewReceivables(string isManager)
        {

            if (isManager == "true")
            {
                return View("ReviewReceivablesView", _invoices.Invoices);
            }
            else {
                return RedirectToAction("Index","UserAccounts");
            }
            
            
        }
    }
}