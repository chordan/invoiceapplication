﻿using InvoiceApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace InvoiceApplication.Persistence
{
    /// <summary>
    /// This sets up the localDb for us
    /// </summary>
    public class InvoiceAppDbContext:DbContext
    {
        public InvoiceAppDbContext() : base("InvoiceAppDbContext")
        {
        }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<User> Users { get; set; }
        

    }
}