﻿using InvoiceApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApplication.Persistence
{
    interface IInvoiceRepository
    {
        IEnumerable<Invoice> InvoiceList { get; }
        IEnumerable<User> UserList { get; }

        void SaveInvoice(Invoice invoice);
        
        void DeleteInvoice(Invoice invoice);

        User CheckUser(User user);
    }
}