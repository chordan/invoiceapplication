﻿using InvoiceApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceApplication.Persistence
{
    class DatabaseInvoiceRepository : IInvoiceRepository
    {
        private InvoiceAppDbContext _dbContext;

        public DatabaseInvoiceRepository()
        {
            _dbContext = new InvoiceAppDbContext();
        }

        public IEnumerable<Invoice> InvoiceList
        {
            get
            {
                return _dbContext.Invoices;
            }
        }

        public IEnumerable<User> UserList
        {
            get
            {
                return _dbContext.Users;
            }
        }

        public User CheckUser(User user)
        {
            
            if (_dbContext.Users.FirstOrDefault(acc => acc.UserName == user.UserName) != null)
            {
                if (_dbContext.Users.FirstOrDefault(acc => acc.Password == user.Password) != null)
                {
                    User userEntity = _dbContext.Users.FirstOrDefault(acc => acc.UserName == user.UserName);
                    return userEntity;
                }
                else
                {
                    return null;
                }
                    
            }
            else
            {

                return null;
            }
        }

        public void SaveInvoice(Invoice invoice)
        {

            Invoice invoiceEntity = _dbContext.Invoices.Find(invoice.InvoiceNumber);
            //check if this is a new invoice through the invoiceEntity
            //(if it does not match another invoice in the DB)
            if (invoiceEntity==null)
            {
                //add the product to the database
                _dbContext.Invoices.Add(invoice);
            }
            else
            {
                //change the product in the database itself
                invoiceEntity.Change(invoice);
            }
            //apply changes
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Not to be used
        /// </summary>
        /// <param name="invoice"></param>
        public void DeleteInvoice(Invoice invoice)
        {
                //Remove and save
                _dbContext.Invoices.Remove(invoice);
            _dbContext.SaveChanges();
        }

    }
}