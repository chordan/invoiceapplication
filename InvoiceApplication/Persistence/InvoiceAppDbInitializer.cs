﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InvoiceApplication.Models;

namespace InvoiceApplication.Persistence
{
    /// <summary>
    /// This will create some sample invoices if there's not an existing DB
    /// </summary>
    public class InvoiceAppDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<InvoiceAppDbContext>
    {
        protected override void Seed(InvoiceAppDbContext context)
        {

            var Invoices = new List<Invoice>
            {
                        new Invoice
                        {
                            InvoiceNumber=1,
                            ClientName="John Sample",
                            ClientAddress="1234 avenue",
                            DateOfShipment=new DateTime(2016,04,30),
                            PaymentDueDate=new DateTime(2016,06,01),
                            ProductName="Brick",
                            ProductQuantity=2,
                            UnitPrice=10.00m,
                            Currency=CurrencyType.CAD,
                            Paid=false
                        },

                        new Invoice
                        {
                            InvoiceNumber=2,
                            ClientName="Harambe",
                            ClientAddress="Heaven",
                            DateOfShipment=new DateTime(2006,01,01),
                            PaymentDueDate=new DateTime(2016,12,01),
                            ProductName="Bullets",
                            ProductQuantity=1,
                            UnitPrice=1.50m,
                            Currency=CurrencyType.US,
                            Paid=false
                        },

                        new Invoice
                        {
                            InvoiceNumber=3,
                            ClientName="European Man",
                            ClientAddress="Paris",
                            DateOfShipment=new DateTime(2016,02,16),
                            PaymentDueDate=new DateTime(2016,03,16),
                            ProductName="Baguette",
                            ProductQuantity=16,
                            UnitPrice=16.16m,
                            Currency=CurrencyType.EUR,
                            Paid=false
                        },
                    };
            Invoices.ForEach(s => context.Invoices.Add(s));
            context.SaveChanges();

            var Users = new List<User>
            {
                new User {UserId=1,UserName="Ray",Password="yar",Type=UserRole.Manager},
                new User {UserId=2,UserName="Jordan",Password="nadroj",Type=UserRole.User}
            };
            Users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();

        }



    }
}